<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
	// ver1の場合
	function test(){
		console.log('start');
		$.ajax({
			url:'receive.jsp',
			success:function(data){
				console.log(data);

			},
			error:function(data){
				console.log('err:' + data);
			}
		});
		console.log('end');
	}

	// ver新しい
	function test2(){
		console.log('start2');
		$.ajax({
			// url:'receive.jsp',
			url:'http://www.google.co.jp'
		}).then(function(data){
			console.log(data)
		},function(){
			console.log('通信エラー');
		})
	}
</script>
</head>
<body>
	ajaxTest<br />
	<button onClick="test2();">ボタン</button>
</body>
</html>